# -*- coding: utf-8 -*-

{
    'name': 'Rufika Testing Module',
    'version': '1.0',
    'sequence': 100,
    'category': 'Manufacturing',
    'description': """
        This module is for learning odoo development""",
    'depends': [],
    'summary': 'This module is for learning odoo development',
    'website': 'https://www.odoo.com/page/tpm-maintenance-software',
    'data': [],
    'demo': [],
    'installable': True,
    'application': True,
}
